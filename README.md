# Kumparan Test

Skill tes untuk apply posisi front-end di kumparan.

## Cara menjalankan aplikasi

### Di lokal

1. Install package: `npm install`
2. Jalankan aplikasi: `npm start` lalu buka alamat `http://localhost:3000` di browser 
3. Login menggunakan username yang tersedia pada user API, berikut username yang tersedia:

- Bret
- Antonette
- Samantha
- Karianne
- Kamren
- Leopoldo_Corkery
- Elwyn.Skiles
- Maxime_Nienow
- Delphine
- Moriah.Stanton 

### Production

Aplikasi ini di deploy ke netlify, untuk melihatnya [klik disini](https://nifty-keller-40b19f.netlify.com).

### Dear, Tim Recruiter

Saya mencoba untuk secepat mungkin mengerjakannya, maaf kalo masih ada kekurangan seperti belum
semua test dibuat untuk redux action creator & reducer. Saya sangat berharap bisa bekerja di
Kumparan, dan semoga di toleransi. Terima Kasih :)
