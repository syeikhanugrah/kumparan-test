import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAlignJustify, faImage } from "@fortawesome/free-solid-svg-icons";

export const timeline = {
  POSTS: 'TIMELINE_POSTS',
  ALBUMS: 'TIMELINE_ALBUMS'
};

const ProfileTimelineSelector = ({ selectedTimeline, handlerSelectTimeline }) => (
  <div className="tabs is-toggle is-fullwidth">
    <ul>
      <li className={selectedTimeline === timeline.POSTS ? 'is-active' : ''}>
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a href="#" onClick={(e) => handlerSelectTimeline(e, timeline.POSTS)}>
          <span className="icon">
            <FontAwesomeIcon icon={faAlignJustify} />
          </span>
          <span>Postingan</span>
        </a>
      </li>
      <li className={selectedTimeline === timeline.ALBUMS ? 'is-active' : ''}>
        {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
        <a href="#" onClick={(e) => handlerSelectTimeline(e, timeline.ALBUMS)}>
          <span className="icon">
            <FontAwesomeIcon icon={faImage} />
          </span>
          <span>Album foto</span>
        </a>
      </li>
    </ul>
  </div>
);

export default ProfileTimelineSelector;
