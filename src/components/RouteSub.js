import React from 'react';
import { Route } from 'react-router-dom';
import PrivateRoute from './PrivateRoute';
import PropTypes from "prop-types";

const RouteSub = (route) => {
  if (!route.secured) {
    return (
      <Route
        path={route.path}
        exact={route.exact}
        render={(props) => (
          <route.component {...props} routes={route.routes} />
        )}
      />
    );
  } else {
    return (
      <PrivateRoute {...route} />
    );
  }
};

RouteSub.propTypes = {
  path: PropTypes.string,
  secured: PropTypes.bool,
};

export default RouteSub;
