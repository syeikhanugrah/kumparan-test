import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from "prop-types";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    localStorage.getItem('user')
      ? <Component {...props} />
      : <Redirect to="/login" />
  )} />
);

PrivateRoute.propTypes = {
  path: PropTypes.string,
  secured: PropTypes.bool,
};

export default PrivateRoute;
