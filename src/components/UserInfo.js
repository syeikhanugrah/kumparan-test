import React from 'react';
import { Link } from 'react-router-dom';

const UserInfo = ({ user }) => (
  <div className="box">
    <div className="media">
      <div className="media-content">
        <div className="content">
          <div className="columns">
            <div className="column">
              <div>
                <Link to={`/profile/${user ? user.id : ''}`}>
                  <strong>{user ? user.name : ''}</strong>
                </Link>
              </div>
              <div>{user ? user.username : ''} - {user ? user.email : ''}</div>
            </div>
          </div>

          <div className="columns">
            <div className="column user-info">
              <div>Phone: {user ? user.phone : ''}</div>
              <div>Website: {user ? user.website: ''}</div>
              <div>
                Address: {user ? user.address.street : ''}, {user ? user.address.suite : ''}, {user ? user.address.city: ''}
              </div>
            </div>
            <div className="column company-info has-text-right">
              <div>{user ? user.company.name : ''}</div>
              <div>{user ? user.company.catchPhrase : ''}</div>
              <div>{user ? user.company.bs : ''}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default UserInfo;
