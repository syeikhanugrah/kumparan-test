import React from 'react';
import { Link } from 'react-router-dom';

const Album = ({ album }) => (
  <div className="box">
    <div className="media">
      <div className="media-content">
        <Link to={`/album/${album.id}`}>{album.title}</Link>
      </div>
    </div>
  </div>
);

export default Album;
