import React, { Component } from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { userActions } from "../redux/actions/user";
import PropTypes from 'prop-types';

class Header extends Component {
  state = {
    menuToggled: false,
  };

  handleToggleMenu = () => {
    this.setState({menuToggled: !this.state.menuToggled});
  };

  handleLogout = () => {
    const { dispatch } = this.props;

    dispatch(userActions.logout());
  };

  render () {
    const { authentication } = this.props;
    const loggedIn = authentication && authentication.loggedIn;
    const userId = loggedIn ? authentication.user.id : null;

    return (
      <nav className="navbar is-light" role="navigation" aria-label="main navigation">
        <div className="container">
          <div className="navbar-brand">
            <div className="navbar-item">Feed - Kumparan Test</div>
            {loggedIn &&
              <div
                className={`navbar-burger burger ${this.state.menuToggled ? "is-active" : ""}`}
                onClick={this.handleToggleMenu}
              >
                <span></span>
                <span></span>
                <span></span>
              </div>
            }
          </div>

          {loggedIn &&
            <div className={`navbar-menu ${this.state.menuToggled ? "is-active" : ""}`}>
              <div className="navbar-end">
                <Link to="/home" className="navbar-item">Beranda</Link>
                <Link to={`/profile/${userId}`} className="navbar-item">Profil</Link>
                <Link to={`/peoples`} className="navbar-item">Orang-orang</Link>
                <div className="navbar-item">
                  <button className="button is-light" onClick={this.handleLogout}>Logout</button>
                </div>
              </div>
            </div>
          }
        </div>
      </nav>
    );
  }
}

Header.propTypes = {
  authentication: PropTypes.object,
  notification: PropTypes.object,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(Header);
