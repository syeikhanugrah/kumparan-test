import React from 'react';
import Post from "./Post";
import {timeline} from "./ProfileTimelineSelector";
import Album from "./Album";

const ProfileTimeline = ({ selectedTimeline, posts, albums, entityOwner, handlerDeletePost }) => {
  return (
    <>
      {selectedTimeline === timeline.POSTS
        ? posts.map((post, i) => (
            <Post key={i}
              post={post}
              userId={entityOwner ? entityOwner.id : null}
              handlerDeletePost={handlerDeletePost} />
          ))
        : albums.map((album, i) => (
            <Album key={i} album={album} />
          ))
      }
    </>

  );
};

export default ProfileTimeline;
