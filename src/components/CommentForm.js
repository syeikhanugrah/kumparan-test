import React from 'react';
import { Link } from 'react-router-dom';

const CommentForm = () => (
  <div className="media">
    <div className="media-content">
      <form>
        <div className="field">
          <textarea className="textarea" placeholder="Tambah komentar..." />
        </div>

        <div className="field is-grouped">
          <div className="control">
            <button type="submit" className="button is-primary">Komentar</button>
          </div>
          <div className="control">
            <Link to='/home' className="button is-light">Kembali</Link>
          </div>
        </div>
      </form>
    </div>
  </div>
);

export default CommentForm;
