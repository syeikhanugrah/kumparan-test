import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import '../styles/Post.sass';

const Post = ({ post, userId, handlerDeletePost }) => {
  const [isPostOptionsToggled, setPostOptionsToggled] = useState(false);

  const handleTogglePostOptions = () => {
    setPostOptionsToggled(!isPostOptionsToggled);
  };

  return (
    <div className="box">
      <div className="media">
        <div className="media-content">
          <div className="content">
            <div className="media-content__username">
              <strong>{post.title}</strong>
            </div>
            <div className="media-content__body">
              {post.body}
            </div>
          </div>
        </div>
        <div className="media-right">
          <div
            className={`dropdown is-right ${isPostOptionsToggled ? 'is-active' : ''}`}>
            <div className="dropdown-trigger">
              <button
                className="button"
                aria-haspopup="true"
                aria-controls="post-options"
                onClick={handleTogglePostOptions}>
                <span className="icon is-small">
                  <FontAwesomeIcon icon={faChevronDown} />
                </span>
              </button>
            </div>
            <div className="dropdown-menu" id="post-options" role="menu">
              <div className="dropdown-content">
                <Link to={`/post/${post.id}`} className="dropdown-item">Detail</Link>
                {post.userId === userId &&
                  <>
                    <Link to={`/post/${post.id}/edit`} className="dropdown-item">Edit</Link>
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a href="#" className="dropdown-item" onClick={(e) => handlerDeletePost(e, post.id)}>Hapus</a>
                  </>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

Post.propTypes = {
  post: PropTypes.shape({
    userId: PropTypes.number,
    id: PropTypes.number,
    title: PropTypes.string,
    body: PropTypes.string
  })
};

export default Post;
