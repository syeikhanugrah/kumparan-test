import React from 'react';

const Comment = ({ comment }) => (
  <div className="media">
    <div className="media-left"></div>
    <div className="media-content">
      <div className="media-comments__username">
        <strong>{comment.email}</strong>
      </div>
      <div className="media-comments__body">
        {comment.body}
      </div>
    </div>
  </div>
);

export default Comment;
