import React from 'react';
import { Link } from 'react-router-dom';

const PostForm = ({ post, handlerPostSubmit, handlerFieldChange }) => (
  <div className="box">
    <div className="media">
      <div className="media-left">Kamu</div>
      <div className="media-content">
        <form id="post-form" onSubmit={handlerPostSubmit}>
          <div className="field">
            <input
              type="text"
              name="submittedPostTitle"
              className="input"
              placeholder={post ? post.title : 'Judul'}
              onBlur={handlerFieldChange} />
          </div>
          <div className="field">
            <textarea
              name="submittedPostBody"
              className="textarea"
              placeholder={post ? post.body : 'Apa yang ingin di posting?'}
              onBlur={handlerFieldChange} />
          </div>


          <div className="field is-grouped">
            <div className="control">
              <button type="submit" className="button is-primary">{post ? 'Ubah' : 'Post'}</button>
            </div>
            <div className="control">
              {post &&
                <Link to='/home' className="button is-light">Kembali</Link>
              }
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
);

export default PostForm;
