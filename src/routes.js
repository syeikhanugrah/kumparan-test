import Login from "./views/Login";
import Home from "./views/Home";
import PostDetail from './views/PostDetail';
import PostEdit from './views/PostEdit';
import UserProfile from './views/UserProfile';
import AlbumDetail from './views/AlbumDetail';
import Peoples from './views/Peoples';

export default [
  {
    path: '/home',
    component: Home,
    secured: true
  },
  {
    path: '/login',
    component: Login,
    secured: false
  },
  {
    exact: true,
    path: '/post/:postId',
    component: PostDetail,
    secured: true
  },
  {
    path: '/post/:postId/edit',
    component: PostEdit,
    secured: true
  },
  {
    path: '/profile/:userId',
    component: UserProfile,
    secured: true
  },
  {
    path: '/album/:albumId',
    component: AlbumDetail,
    secured: true
  },
  {
    path: '/peoples',
    component: Peoples,
    secured: true
  },
];
