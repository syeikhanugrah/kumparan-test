import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import Comment from '../components/Comment';
import {connect} from 'react-redux';
import {postActions} from '../redux/actions/post';
import CommentForm from '../components/CommentForm';
import { Link } from "react-router-dom";

class PostDetail extends Component {
  constructor(props) {
    super(props);

    this.state = {
      comments: null,
      user: null,
      isPostOptionsToggled: false,
    };

    const { dispatch, match } = this.props;
    const { postId } = match.params;

    dispatch(postActions.fetchOne(postId));
  }

  componentDidMount() {
    const { postId } = this.props.match.params;

    fetch(`https://jsonplaceholder.typicode.com/comments?postId=${postId}`)
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        this.setState({ comments: response });
      })
      .catch(error => {
        console.log(error);
      });
  }

  handleTogglePostOptions = () => {
    this.setState({ ...this.state, isPostOptionsToggled: !this.state.isPostOptionsToggled });
  };

  handleDeletePost = (e, postId) => {
    e.preventDefault();

    const { dispatch } = this.props;

    dispatch(postActions.deletePost(postId));
  };

  render () {
    const post = this.props.post.post;
    const userId = this.props.authentication.user.id;
    const { isPostOptionsToggled } = this.state;

    return (
      <div className="box">
        <div className="media">
          <div className="media-content">
            <div className="content">
              <div className="media-content__username">
                <strong>{post ? post.title : ''}</strong>
              </div>
              <div className="media-content__body">
                {post ? post.body : ''}
              </div>
            </div>
            <h3 className="">Komentar</h3>
            {this.state.comments
              ? this.state.comments.map((comment, i)=> (
                <Comment key={i} comment={comment} />
              ))
              : ''
            }
            <CommentForm />
          </div>
          <div className="media-right">
            <div
              className={`dropdown is-right ${isPostOptionsToggled ? 'is-active' : ''}`}>
              <div className="dropdown-trigger">
                <button
                  className="button"
                  aria-haspopup="true"
                  aria-controls="post-options"
                  onClick={this.handleTogglePostOptions}>
                  <span className="icon is-small">
                    <FontAwesomeIcon icon={faChevronDown} />
                  </span>
                </button>
              </div>
              <div className="dropdown-menu" id="post-options" role="menu">
                <div className="dropdown-content">
                  {post && post.userId === userId &&
                    <>
                      <Link to={`/post/${post.id}/edit`} className="dropdown-item">Edit</Link>
                      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                      <a href="#" className="dropdown-item" onClick={(e) => this.handleDeletePost(e, post.id)}>Hapus</a>
                    </>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(PostDetail);
