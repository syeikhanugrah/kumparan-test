import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { userActions } from '../redux/actions/user';

import '../styles/Login.sass';

class Login extends Component {
  constructor(props) {
    super(props);

    // reset login status
    this.props.dispatch(userActions.logout());

    this.state = {
      username: '',
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  handleSubmit(e) {
    e.preventDefault();

    this.setState({ submitted: true });

    const { username } = this.state;
    const { dispatch } = this.props;

    if (username) {
      dispatch(userActions.login(username));
    }
  }

  render () {
    const { username, submitted } = this.state;
    return (
      <div id="login-container">
        <div className="login-card">
          <div className="login-card__title">
            <h2>Login</h2>
          </div>

          <div className="login-card__content">
            <form name="form" onSubmit={this.handleSubmit}>
              <div className="field">
                <input
                  name="username"
                  type="text"
                  className={`input ${submitted && !username ? 'is-danger' : ''}`}
                  placeholder="Username" value={username}
                  onChange={this.handleChange}
                />
                {submitted && !username &&
                  <p className="help is-danger">Username tidak boleh kosong</p>
                }
              </div>

              <button className="button is-primary" type="submit">Login</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  username: PropTypes.string,
  submitted: PropTypes.bool,
  dispatch: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(Login);
