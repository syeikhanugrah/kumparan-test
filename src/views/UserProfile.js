import React, { Component } from 'react';
import { connect } from 'react-redux';
import UserInfo from '../components/UserInfo';
import { userActions } from "../redux/actions/user";
import ProfileTimelineSelector, { timeline } from '../components/ProfileTimelineSelector';
import {postActions} from "../redux/actions/post";
import ProfileTimeline from "../components/ProfileTimeline";
import {albumActions} from "../redux/actions/album";

class UserProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTimeline: timeline.POSTS,
    };
  }

  componentDidMount() {
    const { dispatch, match } = this.props;
    const { userId } = match.params;

    dispatch(userActions.getProfile(userId));
    dispatch(postActions.fetchByUser(userId));
    dispatch(albumActions.fetchByUser(userId));
  }

  handleSelectTimeline = (e, selectedTimeline) => {
    e.preventDefault();

    this.setState({ selectedTimeline: selectedTimeline });
  };

  handleDeletePost = (e, postId) => {
    e.preventDefault();

    const { dispatch } = this.props;

    dispatch(postActions.deletePost(postId));
  };

  render () {
    const user = this.props.profile.data;
    const posts = this.props.post.posts;
    const albums = this.props.album.data;
    const entityOwner = this.props.authentication.user;

    return (
      <>
        <UserInfo user={user} />

        <ProfileTimelineSelector
          selectedTimeline={this.state.selectedTimeline}
          handlerSelectTimeline={this.handleSelectTimeline} />

        <ProfileTimeline
          selectedTimeline={this.state.selectedTimeline}
          albums={albums}
          posts={posts}
          entityOwner={entityOwner}
          handlerDeletePost={this.handleDeletePost} />
      </>
    );
  }
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(UserProfile);
