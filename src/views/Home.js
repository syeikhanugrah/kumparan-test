import React, { Component } from 'react';
import { connect } from "react-redux";
import Post from '../components/Post';
import PostForm from "../components/PostForm";
import { postActions } from '../redux/actions/post';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      submittedPostTitle: null,
      submittedPostBody: null,
    };
  }
  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(postActions.fetchAll());
  }

  handleFieldChange = (e) => {
    const { name, value } = e.target;

    this.setState({ [name]: value });
  };

  handlePostSubmit = (e) => {
    e.preventDefault();

    const { dispatch, authentication } = this.props;
    const { submittedPostTitle, submittedPostBody } = this.state;

    dispatch(postActions.addPost({
      title: submittedPostTitle,
      body: submittedPostBody,
      userId: authentication.user.id
    }));

    document.getElementById("post-form").reset();
  };

  handleDeletePost = (e, postId) => {
    e.preventDefault();

    const { dispatch } = this.props;

    dispatch(postActions.deletePost(postId));
  };

  render() {
    const posts = this.props.post.posts;
    const userId = this.props.authentication.user.id;
    const postData = this.props.post.postData;

    return (
      <>
        <PostForm
          handlerPostSubmit={this.handlePostSubmit}
          handlerFieldChange={this.handleFieldChange} />

        {postData.length !== 0 && postData.reverse().map((post, i) => (
            <Post
              key={i}
              post={{ id: post.id, title: post.title, body: post.body, userId: post.userId }}
              userId={userId}
              handlerDeletePost={this.handleDeletePost} />
          ))
        }

        {posts.map((post, i) => (
          <Post key={i} post={post} userId={userId} handlerDeletePost={this.handleDeletePost} />
        ))}
      </>
    );
  }
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(Home);
