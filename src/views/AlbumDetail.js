import React, {Component} from 'react';
import {connect} from 'react-redux';
import {albumActions} from "../redux/actions/album";
import Gallery from 'react-grid-gallery';

class AlbumDetail extends Component {
  componentDidMount() {
    const { dispatch, match } = this.props;
    const { albumId } = match.params;

    dispatch(albumActions.fetchPhotos(albumId));
  }

  render () {
    const newArrayPhotos = [];
    const oldArrayPhotos = this.props.album.photos;

    // eslint-disable-next-line array-callback-return
    oldArrayPhotos.map(photo => {
      newArrayPhotos.push({
        src: photo.url,
        thumbnail: photo.thumbnailUrl,
        thumbnailWidth: 200,
        thumbnailHeight: 200,
        caption: photo.title,
      });
    });

    return (
      <Gallery images={newArrayPhotos} />
    );
  }
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(AlbumDetail);
