import React, {Component} from 'react';
import {connect} from 'react-redux';
import {userActions} from "../redux/actions/user";
import UserInfo from "../components/UserInfo";

class Peoples extends Component {
  componentDidMount() {
    const { dispatch } = this.props;

    dispatch(userActions.getAll());
  }

  render () {
    const users = this.props.users.data;

    return (
      <>
        {users && users.map((user, i) => <UserInfo key={i} user={user} />)}
      </>
    );
  }
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(Peoples);
