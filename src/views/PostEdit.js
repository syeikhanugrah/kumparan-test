import React, { Component } from 'react';
import {connect} from 'react-redux';
import {postActions} from '../redux/actions/post';
import PostForm from "../components/PostForm";

class PostEdit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      submittedPostTitle: null,
      submittedPostBody: null
    };

    const { dispatch, match } = this.props;
    const { postId } = match.params;

    dispatch(postActions.fetchOne(postId));
  }

  handleFieldChange = (e) => {
    const { name, value } = e.target;

    this.setState({ [name]: value });
  };

  handlePostSubmit = (e) => {
    e.preventDefault();

    const { dispatch, authentication, match } = this.props;
    const { postId } = match.params;
    const { submittedPostTitle, submittedPostBody } = this.state;

    dispatch(postActions.editPost({
      id: postId,
      title: submittedPostTitle,
      body: submittedPostBody,
      userId: authentication.user.id
    }));
  };

  render () {
    const post = this.props.post.post;

    return (
      <PostForm
        post={post}
        handlerPostSubmit={this.handlePostSubmit}
        handlerFieldChange={this.handleFieldChange} />
    );
  }
}

const mapStateToProps = state => state;

export default connect(mapStateToProps)(PostEdit);
