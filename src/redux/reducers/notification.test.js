import { notification as notificationReducer } from './notification';
import { notificationType } from '../types';

describe('Notification reducers', () => {
  it('returns initial state', () => {
    expect(notificationReducer(undefined, {})).toEqual({});
  });

  it('should handle NOTIFICATION_SUCCESS', () => {
    const action = {
      message: 'Success message',
      type: notificationType.SUCCESS
    };

    expect(notificationReducer({}, action)).toEqual({
      message: 'Success message',
      type: 'is-success'
    });
  });

  it('should handle NOTIFICATION_ERROR', () => {
    const action = {
      message: 'Error message',
      type: notificationType.ERROR
    };

    expect(notificationReducer({}, action)).toEqual({
      message: 'Error message',
      type: 'is-danger'
    });
  });

  it('should handle NOTIFICATION_CLEAR', () => {
    const action = {
      type: notificationType.CLEAR
    };

    expect(notificationReducer({}, action)).toEqual({});
  });
});
