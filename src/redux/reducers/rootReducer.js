import { combineReducers } from 'redux';
import { authentication, profile, users } from "./user";
import { notification } from './notification';
import { post } from './post';
import { album } from './album';

const rootReducer = combineReducers({
  authentication,
  notification,
  post,
  profile,
  album,
  users
});

export default rootReducer;
