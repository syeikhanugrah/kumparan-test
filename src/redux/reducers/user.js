import { userType } from '../types';

const user = JSON.parse(localStorage.getItem('user'));
const authInitialState = user ? { loggedIn: true, user } : {};

export const authentication = (state = authInitialState, action) => {
  switch (action.type) {
    case userType.LOGIN_REQUEST:
      return {
        user: action.user
      };
    case userType.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userType.LOGIN_FAILURE:
      return {};
    case userType.LOGOUT:
      return {};
    default:
      return state
  }
};

const profileInitialState = { pending: null, error: null, data: null };

export const profile = (state = profileInitialState, action) => {
  switch (action.type) {
    case userType.GET_PROFILE_PENDING:
      return {
        ...state,
        pending: true,
      };
    case userType.GET_PROFILE_SUCCESS:
      return {
        ...state,
        data: action.data,
        pending: false
      };
    case userType.GET_PROFILE_FAILURE:
      return {
        ...state,
        error: action.error,
        pending: null
      };
    default:
      return state
  }
};

const usersInitialState = { pending: null, error: null, data: null };

export const users = (state = usersInitialState, action) => {
  switch (action.type) {
    case userType.GET_ALL_PENDING:
      return {
        ...state,
        pending: true,
      };
    case userType.GET_ALL_SUCCESS:
      return {
        ...state,
        data: action.data,
        pending: false
      };
    case userType.GET_ALL_FAILURE:
      return {
        ...state,
        error: action.error,
        pending: null
      };
    default:
      return state
  }
};
