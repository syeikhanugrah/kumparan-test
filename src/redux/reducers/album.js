import { albumType } from '../types';

const initialState = {
  pending: null,
  data: [],
  photos: [],
  error: null
};

export const album = (state = initialState, action) => {
  switch (action.type) {
    case albumType.FETCH_BY_USER_PENDING:
      return {
        ...state,
        pending: true
      };
    case albumType.FETCH_BY_USER_SUCCESS:
      return {
        ...state,
        pending: false,
        data: action.data
      };
    case albumType.FETCH_BY_USER_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      };
    case albumType.FETCH_PHOTOS_PENDING:
      return {
        ...state,
        pending: true
      };
    case albumType.FETCH_PHOTOS_SUCCESS:
      return {
        ...state,
        pending: false,
        photos: action.photos
      };
    case albumType.FETCH_PHOTOS_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      };
    default:
      return state
  }
};
