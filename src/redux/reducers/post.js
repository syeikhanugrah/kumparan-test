import { postType } from '../types';

const initialState = {
  pending: null,
  posts: [],
  post: null,
  postData: [],
  postDataEdit: null,
  error: null
};

export const post = (state = initialState, action) => {
  switch(action.type) {
    case postType.FETCH_ALL_PENDING:
      return {
        ...state,
        pending: true
      };
    case postType.FETCH_ALL_SUCCESS:
      return {
        ...state,
        pending: false,
        posts: action.posts
      };
    case postType.FETCH_ALL_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      };
    case postType.FETCH_ONE_PENDING:
      return {
        ...state,
        pending: true,
      };
    case postType.FETCH_ONE_SUCCESS:
      return {
        ...state,
        pending: false,
        post: action.post
      };
    case postType.FETCH_ONE_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      };
    case postType.FETCH_BY_USER_PENDING:
      return {
        ...state,
        pending: true
      };
    case postType.FETCH_BY_USER_SUCCESS:
      return {
        ...state,
        pending: false,
        posts: action.posts
      };
    case postType.FETCH_BY_USER_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      };
    case postType.ADD_PENDING:
      return {
        ...state,
        pending: true,
      };
    case postType.ADD_SUCCESS:
      return {
        ...state,
        pending: false,
        postData: [...state.postData, action.postData]
      };
    case postType.ADD_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      };
    case postType.EDIT_PENDING:
      return {
        ...state,
        pending: true,
      };
    case postType.EDIT_SUCCESS:
      return {
        ...state,
        pending: false,
        postDataEdit: action.postDataEdit
      };
    case postType.EDIT_FAILURE:
      return {
        ...state,
        pending: false,
        error: action.error
      };
    default:
      return state;
  }
};
