import { notificationType } from '../types';

export const notification = (state = {}, action) => {
  switch (action.type) {
    case notificationType.SUCCESS:
      return {
        type: 'is-success',
        message: action.message
      };
    case notificationType.ERROR:
      return {
        type: 'is-danger',
        message: action.message
      };
    case notificationType.CLEAR:
      return {};
    default:
      return state
  }
};
