import { userType} from '../types';
import { history } from '../../helpers/history';
import { notificationActions } from './notification';

const BASE_URL = 'https://jsonplaceholder.typicode.com';

const login = username => {
  const request = user => ({ type: userType.LOGIN_REQUEST, user });
  const success = user => ({ type: userType.LOGIN_SUCCESS, user });
  const failure = error => ({ type: userType.LOGIN_FAILURE, error });

  return dispatch => {
    dispatch(request({ username }));

    fetch(`${BASE_URL}/users`)
      .then(response => response.json())
      .then(response => {
          const user = response.find(user => user.username === username);

          if (user === undefined) {
            const errorMessage = 'Username tidak ditemukan';

            dispatch(failure(errorMessage));
            dispatch(notificationActions.error(errorMessage))
          } else {
            localStorage.setItem('user', JSON.stringify(user));

            dispatch(success(user));

            history.push('/home');
          }
      })
      .catch(() => {
        const errorMessage = 'Kesalahan tidak diketahui';

        dispatch(failure(errorMessage));
        dispatch(notificationActions.error(errorMessage))
      })
    ;
  };
};

const logout = () => {
  // remove user from local storage to log user out
  localStorage.removeItem('user');

  return { type: userType.LOGOUT };
};

const getProfilePending = () => {
  return {
    type: userType.GET_PROFILE_PENDING,
    pending: true,
  };
};

const getProfileSuccess = (data) => {
  return {
    type: userType.GET_PROFILE_SUCCESS,
    data: data,
    pending: false
  };
};

const getProfileFailure = (error) => {
  return {
    type: userType.GET_PROFILE_SUCCESS,
    pending: false,
    error: error
  };
};

const getProfile = (userId) => {
  return dispatch => {
    dispatch(getProfilePending());

    fetch(`${BASE_URL}/users/${userId}`)
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        dispatch(getProfileSuccess(response));
      })
      .catch(error => {
        dispatch(getProfileFailure(error));
      })
  };
};

const getAllPending = () => {
  return {
    type: userType.GET_ALL_PENDING,
    pending: true,
  };
};

const getAllSuccess = (data) => {
  return {
    type: userType.GET_ALL_SUCCESS,
    data: data,
    pending: false
  };
};

const getAllFailure = (error) => {
  return {
    type: userType.GET_ALL_SUCCESS,
    pending: false,
    error: error
  };
};

const getAll = () => {
  return dispatch => {
    dispatch(getAllPending());

    fetch(`${BASE_URL}/users`)
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        dispatch(getAllSuccess(response));
      })
      .catch(error => {
        dispatch(getAllFailure(error));
      })
  };
};

export const userActions = {
  login,
  logout,
  getProfile,
  getAll
};
