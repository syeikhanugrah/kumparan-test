import { albumType } from '../types';

const BASE_URL = 'https://jsonplaceholder.typicode.com';

const fetchByUserSuccess = (data) => {
  return { type: albumType.FETCH_BY_USER_SUCCESS, data: data };
};

const fetchByUserFailure = (error) => {
  return { type: albumType.FETCH_BY_USER_FAILURE, error: error };
};

const fetchByUserPending = () => {
  return { type: albumType.FETCH_BY_USER_PENDING };
};

const fetchPhotosSuccess = (photos) => {
  return { type: albumType.FETCH_PHOTOS_SUCCESS, photos: photos };
};

const fetchPhotosFailure = (error) => {
  return { type: albumType.FETCH_PHOTOS_FAILURE, error: error };
};

const fetchPhotosPending = () => {
  return { type: albumType.FETCH_PHOTOS_PENDING };
};

const fetchByUser = userId => {
  return dispatch => {
    dispatch(fetchByUserPending());

    fetch(`${BASE_URL}/albums?userId=${userId}`)
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        dispatch(fetchByUserSuccess(response));
      })
      .catch(error => {
        dispatch(fetchByUserFailure(error));
      })
    ;
  }
};

const fetchPhotos = albumId => {
  return dispatch => {
    dispatch(fetchPhotosPending());

    fetch(`${BASE_URL}/photos?albumId=${albumId}`)
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        dispatch(fetchPhotosSuccess(response));
      })
      .catch(error => {
        dispatch(fetchPhotosFailure(error));
      })
    ;
  }
};

export const albumActions = { fetchByUser, fetchPhotos };
