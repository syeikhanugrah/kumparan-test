import { notificationActions } from './notification';
import { notificationType } from '../types';

describe('Notification actions', () => {
  it('should create an action to add a success notification', () => {
    const message = 'Success message';

    const expectedAction = {
      type: notificationType.SUCCESS,
      message
    };

    expect(notificationActions.success(message)).toEqual(expectedAction);
  });

  it('should create an action to add a error notification', () => {
    const message = 'Error message';

    const expectedAction = {
      type: notificationType.ERROR,
      message
    };

    expect(notificationActions.error(message)).toEqual(expectedAction);
  });

  it('should create an action to clear notification', function () {
    const expectedAction = { type: notificationType.CLEAR };

    expect(notificationActions.clear()).toEqual(expectedAction);
  });
});
