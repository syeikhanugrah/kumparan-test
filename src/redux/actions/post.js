import { postType } from '../types';
import { history } from '../../helpers/history';
import { notificationActions } from './notification';

const BASE_URL = 'https://jsonplaceholder.typicode.com';

const fetchAllSuccess = (posts) => {
  return { type: postType.FETCH_ALL_SUCCESS, posts: posts };
};

const fetchAllFailure = (error) => {
  return { type: postType.FETCH_ALL_FAILURE, error: error };
};

const fetchAllPending = () => {
  return { type: postType.FETCH_ALL_PENDING };
};

const fetchOneSuccess = (post) => {
  return { type: postType.FETCH_ONE_SUCCESS, post: post };
};

const fetchOneFailure = (error) => {
  return { type: postType.FETCH_ONE_FAILURE, error: error };
};

const fetchOnePending = () => {
  return { type: postType.FETCH_ONE_PENDING };
};

const fetchByUserSuccess = (posts) => {
  return { type: postType.FETCH_BY_USER_SUCCESS, posts: posts };
};

const fetchByUserFailure = (error) => {
  return { type: postType.FETCH_BY_USER_FAILURE, error: error };
};

const fetchByUserPending = () => {
  return { type: postType.FETCH_BY_USER_PENDING };
};

const addPostSuccess = (postData) => {
  return { type: postType.ADD_SUCCESS, postData: postData };
};

const addPostFailure = (error) => {
  return { type: postType.ADD_FAILURE, error: error };
};

const addPostPending = () => {
  return { type: postType.ADD_PENDING };
};

const editPostSuccess = (postDataEdit) => {
  return { type: postType.EDIT_SUCCESS, postDataEdit: postDataEdit };
};

const editPostFailure = (error) => {
  return { type: postType.EDIT_FAILURE, error: error };
};

const editPostPending = () => {
  return { type: postType.EDIT_PENDING };
};

const deletePostSuccess = () => {
  return { type: postType.DELETE_SUCCESS };
};

const deletePostFailure = (error) => {
  return { type: postType.DELETE_FAILURE, error: error };
};

const deletePostPending = () => {
  return { type: postType.DELETE_PENDING };
};

const fetchAll = () => {
  return dispatch => {
    dispatch(fetchAllPending());

    fetch(`${BASE_URL}/posts`)
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        dispatch(fetchAllSuccess(response));
      })
      .catch(error => {
        dispatch(fetchAllFailure(error));
      })
    ;
  }
};

const fetchOne = postId => {
  return dispatch => {
    dispatch(fetchOnePending());

    fetch(`${BASE_URL}/posts/${postId}`)
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        dispatch(fetchOneSuccess(response));
      })
      .catch(error => {
        dispatch(fetchOneFailure(error));
      })
    ;
  }
};

const fetchByUser = userId => {
  return dispatch => {
    dispatch(fetchByUserPending());

    fetch(`${BASE_URL}/posts?userId=${userId}`)
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        dispatch(fetchByUserSuccess(response));
      })
      .catch(error => {
        dispatch(fetchByUserFailure(error));
      })
    ;
  }
};

const addPost = postData => {
  return dispatch => {
    dispatch(addPostPending());

    fetch(`${BASE_URL}/posts`, {
        method: 'POST',
        body: JSON.stringify({
          title: postData.title,
          body: postData.body,
          userId: postData.userId
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      })
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        dispatch(addPostSuccess(response));
        dispatch(notificationActions.success('Berhasil memposting postingan kamu'));
      })
      .catch(error => {
        dispatch(addPostFailure(error));
        dispatch(notificationActions.error('Gagal memposting postingan kamu'));
      });
  }
};

const editPost = postData => {
  return dispatch => {
    dispatch(editPostPending());

    fetch(`${BASE_URL}/posts/${postData.id}`, {
        method: 'PUT',
        body: JSON.stringify({
          id: postData.id,
          title: postData.title,
          body: postData.body,
          userId: postData.userId
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8"
        }
      })
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        dispatch(editPostSuccess(response));
        dispatch(notificationActions.success('Berhasil mengubah postingan'));

        setTimeout(() => {
          history.push('/home');
        }, 1000);
      })
      .catch(error => {
        dispatch(editPostFailure(error));
        dispatch(notificationActions.error('Gagal mengubah postingan'));
      });
  }
};

const deletePost = postId => {
  return dispatch => {
    dispatch(deletePostPending());

    fetch(`${BASE_URL}/posts/${postId}`, { method: 'DELETE' })
      .then(response => response.json())
      .then(response => {
        if (response.error) {
          throw(response.error);
        }

        dispatch(deletePostSuccess());
        dispatch(notificationActions.success('Berhasil menghapus postingan'));

        setTimeout(() => {
          window.location.href = '/home';
        }, 1000);

      })
      .catch(error => {
        dispatch(deletePostFailure(error));
      });
  }
};

export const postActions = { fetchAll, fetchOne, fetchByUser, addPost, editPost, deletePost };
