import { notificationType } from '../types';

const success = (message) => {
  return { type: notificationType.SUCCESS, message };
};

const error = (message) => {
  return { type: notificationType.ERROR, message };
};

const clear = () => {
  return { type: notificationType.CLEAR };
};

export const notificationActions = {
  success,
  error,
  clear
};
