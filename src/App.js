import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Switch } from 'react-router-dom';
import routes from './routes';
import RouteSub from './components/RouteSub';
import Header from './components/Header';
import { history } from './helpers/history';
import { notificationActions } from './redux/actions/notification';

class App extends Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
      // clear notification on location change
      dispatch(notificationActions.clear());
    });
  }

  render () {
    const { notification } = this.props;

    return (
      <>
        <Header />

        <section id="content" className="section">
          <div className="container">
            {notification.message &&
              <div
                className={`notification ${notification.type}`}>
                {notification.message}
              </div>
            }

            <Switch>
              {routes.map((route, i) => (
                <RouteSub key={i} {...route} />
              ))}
              {/* Jika tidak ada route yang cocok, alihkan ke halaman login */}
              <Redirect to="/login" />
            </Switch>
          </div>
        </section>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return state;
};

export default connect(mapStateToProps)(App);
